/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.bth.swatkats.letstalk.connection.packet.message;

import java.io.File;
import java.sql.Timestamp;
import se.bth.swatkats.letstalk.connection.packet.Packet;

/**
 *
 * @author ThePo
 */
public class VoiceMessage extends Packet {

    private File voicefile;
    
    private Timestamp time;
    
    public VoiceMessage(int receiver, String senderip, File voicefile) {
        super(receiver, senderip);
        this.voicefile = voicefile;
        this.time = new Timestamp(System.currentTimeMillis());
    }
}
