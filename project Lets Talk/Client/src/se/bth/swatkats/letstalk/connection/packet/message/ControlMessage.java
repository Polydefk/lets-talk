/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.bth.swatkats.letstalk.connection.packet.message;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ThePo
 */
public class ControlMessage extends Message {

    public enum ControlState {
        INITIATE, ACCEPT, DECLINE, TERMINATE
    };
    private ControlState state;
    private InetAddress ipToConnect;
    private String pubKey;
    private int port;

//    public ControlMessage(ControlState aState, String aIp,
//            String aPubKey, String aMyport, String username,
//            int senderid, Timestamp time, String senderip, int receiver) {
//        super(senderid, username, time, receiver, -1, senderip);
//        this.state = aState;
//        this.ip = aIp;
//        this.pubKey = aPubKey;
//        this.port = aMyport;
//    }
//
    public ControlMessage (ControlState state, InetAddress ipToConnect, int port, int receiver, int conversationId) {
        super(receiver, conversationId);
        this.state = state;
        this.ipToConnect = ipToConnect;
        this.port = port;
    }

    public ControlState getControlState() {
        return state;
    }

    public void setControlState(ControlState controlState) {
        this.state = controlState;
    }

    public InetAddress getIp() {
        return ipToConnect;
    }

    public void setIp(String ipToConnect) {
        try {
            this.ipToConnect = InetAddress.getByName(ipToConnect);
        } catch (UnknownHostException ex) {
            Logger.getLogger(ControlMessage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getPubkey() {
        return pubKey;
    }

    public void setPubkey(String pubKey) {
        this.pubKey = pubKey;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int myport) {
        this.port = myport;
    }
}
