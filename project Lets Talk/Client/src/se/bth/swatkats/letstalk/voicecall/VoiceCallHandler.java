package se.bth.swatkats.letstalk.voicecall;

import javax.sound.sampled.AudioFormat;
import javax.swing.JOptionPane;
import se.bth.swatkats.letstalk.connection.GuiHandler;
import se.bth.swatkats.letstalk.connection.packet.message.ControlMessage;
import se.bth.swatkats.letstalk.connection.packet.message.ControlMessage.ControlState;

/**
 *
 * @author Chris
 */
public class VoiceCallHandler {
    
    public enum HandlerState {
        INACTIVE, ACTIVE
    };

    /**
     *  Defines the size of audio packets.
     *
     *  TODO: change packet size depending on connection quality.
     *
     */
    public static final int PACKET_SIZE = 4096;
    /*public static final AudioFormat FORMAT = new AudioFormat(AudioFormat.Encoding.ALAW,
                    22000, 64, 2, 128, 22000, false);*/
    public static final AudioFormat FORMAT = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED,
                    44100, 16, 2, 4, 44100, false);

    private static final int LISTEN_PORT = 6112;
    private static final int CONNECT_PORT = 6113;
    
    private HandlerState state = HandlerState.INACTIVE;

    private VoiceCallListener voiceCallListenerThread = null;
    private VoiceCallRecorder voiceCallRecorderThread = null;

    private static volatile VoiceCallHandler instance;

    public static VoiceCallHandler getInstance() {
        if (instance == null) {
            synchronized (VoiceCallHandler.class) {
                if (instance == null) {
                    instance = new VoiceCallHandler();
                }
            }
        }
        return instance;
    }
    
    public HandlerState getHandlerState() {
        return state;
    }

    public void incomingControlMessage(ControlMessage cm) {
        
        System.out.println("incoming control message: " + cm.getControlState().toString());

        if (cm.getControlState() == ControlState.INITIATE) {
            incomingCallInitiate(cm);
        } else if (cm.getControlState() == ControlState.ACCEPT) {
            acceptCallMessage(cm);
        } else if (cm.getControlState() == ControlState.DECLINE) {
            declineCallMessage(cm);
        } else if (cm.getControlState() == ControlState.TERMINATE) {
            terminateCallMessage(cm);
        }

    }

    public void outgoingCallInit(int receiverId, int convId) {
        
        System.out.println("outgoing call init");
        System.out.println("sending initiate message to: " + GuiHandler.getInstance().getUser().getIp().toString());
        
        ControlMessage cmout = new ControlMessage(ControlState.INITIATE, GuiHandler.getInstance().getUser().getIp(), LISTEN_PORT, receiverId, convId);
        GuiHandler.getInstance().sendMessage(cmout);
        
        System.out.println("starting listener thread");
        
        voiceCallListenerThread = new VoiceCallListener(LISTEN_PORT);
        voiceCallListenerThread.start();
        
        state = HandlerState.ACTIVE;
    }
 
    public void terminateCall(int receiverId, int convId) {
        System.out.println("terminate call");
        System.out.println("sending terminate message to: " + GuiHandler.getInstance().getUser().getIp().toString());
        
        ControlMessage cmout = new ControlMessage(ControlState.TERMINATE, GuiHandler.getInstance().getUser().getIp(), LISTEN_PORT, receiverId, convId);
        GuiHandler.getInstance().sendMessage(cmout);

        System.out.println("terminating listener thread");
        
        voiceCallListenerThread.terminate();
        voiceCallListenerThread = null;
        
        System.out.println("terminating recorder thread");

        voiceCallRecorderThread.terminate();
        voiceCallRecorderThread = null;
        
        state = HandlerState.INACTIVE;
    }

    public void incomingCallInitiate(ControlMessage cm) {
        System.out.println("incoming call init");
        System.out.println("showing message dialog...");
        
        int selection = JOptionPane.showConfirmDialog(null, "Would you like to accept the call?", "Incoming call by ", JOptionPane.YES_NO_OPTION);

        if (selection == JOptionPane.YES_OPTION) {
            System.out.println("sending accept message to: " + GuiHandler.getInstance().getUser().getIp().toString());
            ControlMessage cmout = new ControlMessage(ControlState.ACCEPT, GuiHandler.getInstance().getUser().getIp(), CONNECT_PORT, cm.getSenderid(), cm.getConversationid());
            GuiHandler.getInstance().sendMessage(cmout);

            System.out.println("starting listener thread");
            
            voiceCallListenerThread = new VoiceCallListener(CONNECT_PORT);
            voiceCallListenerThread.start();
            
            System.out.println("starting recorder thread");

            voiceCallRecorderThread = new VoiceCallRecorder(cm.getIp(), cm.getPort());
            voiceCallRecorderThread.start();
            
            state = HandlerState.ACTIVE;

        } else {
            System.out.println("sending decline message to: " + GuiHandler.getInstance().getUser().getIp().toString());
            ControlMessage cmout = new ControlMessage(ControlState.DECLINE, GuiHandler.getInstance().getUser().getIp(), CONNECT_PORT, cm.getSenderid(), cm.getConversationid());
            GuiHandler.getInstance().sendMessage(cmout);
            
            state = HandlerState.INACTIVE;
        }
    }

    public void acceptCallMessage(ControlMessage cm) {
        System.out.println("accept call message");
        System.out.println("starting recorder thread");
        voiceCallRecorderThread = new VoiceCallRecorder(cm.getIp(), cm.getPort());
        voiceCallRecorderThread.start();
        
        state = HandlerState.ACTIVE;
    }

    public void declineCallMessage(ControlMessage cm) {
        System.out.println("decline call message");
        System.out.println("terminating listener thread");
        voiceCallListenerThread.terminate();
        System.out.println("listener thread terminated");
        voiceCallListenerThread = null;
        System.out.println("listener thread is null");
        
        state = HandlerState.INACTIVE;
    }

    public void terminateCallMessage(ControlMessage cm) {
        System.out.println("terminate call message");
        
        System.out.println("terminating listener thread");
        voiceCallListenerThread.terminate();
        voiceCallListenerThread = null;

        System.out.println("terminating recorder thread");
        voiceCallRecorderThread.terminate();
        voiceCallRecorderThread = null;
        
        state = HandlerState.INACTIVE;
    }
    
    
}
