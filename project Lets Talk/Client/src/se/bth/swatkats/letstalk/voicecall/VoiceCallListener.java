package se.bth.swatkats.letstalk.voicecall;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.swing.JOptionPane;

/**
 * Voice call listener thread.
 *
 * This thread handles listening to input packets and playing them locally.
 *
 * @author Chris
 */
public class VoiceCallListener extends Thread {

    private int listenPort;

    private volatile boolean terminate = false;

    public VoiceCallListener(int connectPort) {
        this.listenPort = connectPort;
    }

    public void terminate() {
        terminate = true;
    }

    @Override
    public void run() {
        try {
            byte[] buff = new byte[VoiceCallHandler.PACKET_SIZE];
            DatagramSocket socket = new DatagramSocket(listenPort);
            DatagramPacket packet = new DatagramPacket(buff, buff.length);

            // encoding, sample rate, sample size in bits, channels (2=stereo)
            // frame size, frame rate, bigEndian
            AudioFormat format = VoiceCallHandler.FORMAT;

            DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);
            System.out.println("Listener data line: " + info.toString());

            if (!AudioSystem.isLineSupported(info)) {
                // Fail Inform
                JOptionPane.showMessageDialog(null, "Listener line is not supported!", "Error", JOptionPane.ERROR_MESSAGE);
                socket.close();
                return;
            }

            SourceDataLine sourceLine;

            sourceLine = (SourceDataLine) AudioSystem.getSourceDataLine(format);
            sourceLine.open();      // Open Line
            sourceLine.start();     // Record Line

            //socket.receieve blocks by default
            //A timeout is necessary to allow for termination flag checks
            socket.setSoTimeout(200);

            while (!terminate) {

                try {
                    //Read from socket
                    socket.receive(packet);

                    buff = packet.getData();

                    //Play recieved packet
                    sourceLine.write(buff, 0, buff.length);
                } catch (SocketTimeoutException ex) {
                    //TODO: automatically terminate after some timeouts?
                }

            }

            sourceLine.drain();
            sourceLine.close();     // Close

            socket.close();
            
        } catch (IOException | LineUnavailableException ex) {
            Logger.getLogger(VoiceCallListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
