package se.bth.swatkats.letstalk.voicecall;

import java.awt.HeadlessException;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;
import javax.swing.JOptionPane;

/**
 *
 * @author Chris
 */
public class VoiceCallRecorder extends Thread {

    private InetAddress connectIp;
    private int connectPort;

    private volatile boolean terminate = false;

    public VoiceCallRecorder (InetAddress connectIp, int connectPort) {
        this.connectIp = connectIp;
        this.connectPort = connectPort;
    }

    public void terminate () {
        terminate = true;
    }

    @Override
    public void run () {
        try {
            byte[] buff = new byte[VoiceCallHandler.PACKET_SIZE];
            DatagramSocket socket = new DatagramSocket();
            DatagramPacket packet = new DatagramPacket(buff, buff.length, connectIp, connectPort);;

            // encoding, sample rate, sample size in bits, channels (2=stereo)
            // frame size, frame rate, bigEndian
            AudioFormat format = VoiceCallHandler.FORMAT;

            DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
            System.out.println("Recorder data line: " + info.toString());

            if (!AudioSystem.isLineSupported(info)) {
                // Fail Inform
                JOptionPane.showMessageDialog(null, "Recorder line is not supported!", "Error", JOptionPane.ERROR_MESSAGE);
                socket.close();
                return;
            }

            TargetDataLine targetLine;

            targetLine = (TargetDataLine) AudioSystem.getTargetDataLine(format);
            targetLine.open();      // Open Line

            targetLine.start();     // Record Line

            AudioInputStream audioStream = new AudioInputStream(targetLine);

            while (!terminate) {
                //Read from audio stream
                audioStream.read(buff, 0, buff.length);

                //Set packet data
                packet.setData(buff, 0, buff.length);

                //Send packet
                socket.send(packet);
            }

            targetLine.stop();      // Stop Recording
            targetLine.close();     // Close
            
            socket.close();

        } catch (IOException | HeadlessException | LineUnavailableException ex) {
            Logger.getLogger(VoiceCallRecorder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
